var products = ( function() {

  /* Objeto vacío donde se guardarán los elementos del DOM que vayan a utilizarse */
  var dom = {};

  /* Objeto donde se colocan las variables que vayan a utilizarse */
  var obj = {
    tabs_item_target: 'tabs_item_target',
    metrics: 'metrics',
    insights: 'insights',
    engange: 'engange',
    sectionText: "product-text",
    sectionImg: 'product-img'
  };

  /* Función que llena el objeto dom con los elementos html requeridos */
  var catchDom = function() {
    dom.tabs_item_target = document.getElementsByClassName(obj.tabs_item_target);
    dom.metrics = document.getElementById(obj.metrics);
    dom.insights = document.getElementById(obj.insights);
    dom.engange = document.getElementById(obj.engange);
    dom.sectionText = document.getElementsByClassName(obj.sectionText);
    dom.sectionImg = document.getElementsByClassName(obj.sectionImg);
  };

  /* Función donde se colocan todos los eventos que tendrán los elementos */
  var suscribeEvents = function() {
    /* agrega evento click a la lista de productos*/
    var length = dom.tabs_item_target.length;
    for(var i = 0; i < length; i++){
      dom.tabs_item_target[i].addEventListener('click', events.changeProduct);
    }
  };

  /* Objeto donde se guardan los métodos (o callbacks) de suscribeEvents */
  var events = {
    changeProduct: function() {
      if(this == dom.tabs_item_target[0]){
        dom.metrics.classList.add("is_active");
        dom.insights.classList.remove("is_active");
        dom.engange.classList.remove("is_active");
        dom.tabs_item_target[0].style.color = "#303f9f";
        dom.tabs_item_target[1].style.color = "#b0bec5";
        dom.tabs_item_target[2].style.color = "#b0bec5";
      } else if(this == dom.tabs_item_target[1]){
        dom.metrics.classList.remove("is_active");
        dom.insights.classList.add("is_active");
        dom.engange.classList.remove("is_active");
        dom.tabs_item_target[0].style.color = "#b0bec5";
        dom.tabs_item_target[1].style.color = "#00b8d4";
        dom.tabs_item_target[2].style.color = "#b0bec5";
      } else{
        dom.metrics.classList.remove("is_active");
        dom.insights.classList.remove("is_active");
        dom.engange.classList.add("is_active");
        dom.tabs_item_target[0].style.color = "#b0bec5";
        dom.tabs_item_target[1].style.color = "#b0bec5";
        dom.tabs_item_target[2].style.color = "#ff5722";
      }
    }
  };

  /* Función que inicializa las funciones descritas previamente */
  var initialize = function() {
    catchDom();
    suscribeEvents();
  };

  /* Retornamos un objeto con el método init, haciendo referencia a la función initialize*/
  return {
    init: initialize
  };

})();

products.init();
